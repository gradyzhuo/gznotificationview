//
//  ViewController.swift
//  GZNotificationViewDemo
//
//  Created by Grady Zhuo on 2014/11/15.
//  Copyright (c) 2014年 Grady Zhuo. All rights reserved.
//

import UIKit

class GZMySuccessNotificationView: GZNotifierTemplateView {
    
    
    
}

class ViewController: UIViewController, GZNotifierDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        GZNotifier.defaultNotifier.delegate = self
//        GZNotifier.defaultNotifier.register(nibName: "Hello", inBundle: nil, useIndex: 0, forType: .Success)
        
        
        GZNotifier.defaultNotifier.notificationOffset.y = 64
        
//        UIGraphicsGetImageFromCurrentImageContext()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    
    @IBAction func success(sender:AnyObject){
        
//        GZNotifier.defaultNotifier.show(type: .Success, sampleMessage: "Success Message.", animated: true)
        
        NSNotificationCenter.defaultCenter().postNotificationName(GZNotifierShowSuccessNotificationName, object: nil, userInfo: [kGZNotifierShowNotificationMessage:"Success Message."])
        
        
        
    }
    
    @IBAction func fail(sender:AnyObject){
        GZNotifier.defaultNotifier.show(type: .Failed, sampleMessage: "Failed Message.", animated: true)
        
    }
    
    @IBAction func warning(sender:AnyObject){
        GZNotifier.defaultNotifier.show(type: .Warning, sampleMessage: "Warning Message.", animated: true)
    }
    
    func notifierWillAppear(notifier: GZNotifier, notificationView templateView: GZNotifier.TemplateView) {
        println("templateView:\(templateView)")
    }
    
    
    func notifierPrepareNotificationView(notifier: GZNotifier, type: GZNotifierType, notification: GZNotification, notificationView templateView: GZNotifierTemplateView) {
        templateView.appearInSize.height = 42// = CGSize(width: 640, height: 20)
        println("notifierPrepareNotificationView type:\(type)")
    }
    
    func notifierShouldAutoHiddenBySetting(notifier: GZNotifier, type: GZNotifierType, notification: GZNotification) -> GZNotifierAutoHiddenSetting {
        return GZNotifierAutoHiddenSetting.AutoHiddenAfter(2)
    }
    
}

